import { queryCondition } from '../core/database';

export interface IRead<T> {
  findAll(): Promise<T[]>;
  findById(id: any): Promise<T>;
  find(conditions: queryCondition[]): Promise<T[]>;
}
