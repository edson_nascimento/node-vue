import { Contact } from '../entity/Contact';
import { BaseRepository } from './BaseRepository';

export class ContactRepository extends BaseRepository<Contact> {
  protected tableName(): string {
    return 'contacts';
  }

  isRequireTenant(): boolean {
    return true;
  }
}
