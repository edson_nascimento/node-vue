import { Tenant } from '../entity/Tenant';
import { IRepository } from './IRepository';

export interface ITenantRepository extends IRepository<Tenant> {
  findByDomain(domain: string): Promise<Tenant>;
  findBySlug(slug: string): Promise<Tenant>;
}
