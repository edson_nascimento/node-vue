import { IRead } from './IRead';
import { IWrite } from './IWrite';

export interface IRepository<T> extends IWrite<T>, IRead<T> {
  isRequireTenant(): boolean;
}
