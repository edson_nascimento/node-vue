import { Tenant } from '../entity/Tenant';
import { BaseRepository } from './BaseRepository';
import { ITenantRepository } from './ITenantRepository';

export class TenantRepository extends BaseRepository<Tenant> implements ITenantRepository {
  protected tableName(): string {
    return 'tenant';
  }

  isRequireTenant(): boolean {
    return false;
  }

  async findByDomain(domain: string): Promise<Tenant> {
    return this.queryBuilder(this.tableName()).where('domain', '=', domain).first();
  }

  async findBySlug(slug: string): Promise<Tenant> {
    return this.queryBuilder(this.tableName()).where('slug', '=', slug).first();
  }
}
