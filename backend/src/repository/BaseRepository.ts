import Knex from 'knex';
import { queryCondition } from '../core/database';
import { IRepository } from './IRepository';

export abstract class BaseRepository<T> implements IRepository<T> {
  constructor(protected readonly queryBuilder: Knex) {}

  protected abstract tableName(): string;

  isRequireTenant(): boolean {
    throw new Error('Method not implemented.');
  }

  async create(entity: T): Promise<any> {
    const insert = await this.queryBuilder(this.tableName()).insert(entity).returning('id');
    return this.findById(insert.shift());
  }

  async update(id: string, entity: T): Promise<boolean> {
    await this.queryBuilder(this.tableName()).update(entity).where('id', '=', id);
    return true;
  }

  async delete(id: any): Promise<boolean> {
    return this.queryBuilder(this.tableName()).where('id', '=', id).delete();
  }

  async findById(id: any): Promise<T> {
    return this.queryBuilder(this.tableName()).where('id', '=', id).first();
  }

  async findAll(): Promise<T[]> {
    return this.queryBuilder.table(this.tableName()).select();
  }

  async find(conditions: queryCondition[]): Promise<T[]> {
    const builder = this.queryBuilder.table(this.tableName());

    conditions.forEach((condition) => builder.andWhere(condition.field, condition.operator, condition.value));

    return builder.select();
  }
}
