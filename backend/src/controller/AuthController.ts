import { Request, Response } from 'express';
import { AppException } from '../core/AppException';
import { ContainerDI } from '../core/ContainerDi';
import { getTenantRequest } from '../middleware/auth';
import { AuthService } from '../service/AuthService';

export class AuthController {
  async clientLogin(req: Request, res: Response) {
    const authService = ContainerDI.get<AuthService>('AuthService');

    const tenant = await getTenantRequest(req);

    if (tenant) {
      const { user, password } = req.body;
      const token = await authService.login(tenant, user, password);

      if (token) {
        res.json({
          accessToken: token,
          tokenType: 'Bearer',
        });
      }
    }

    throw new AppException('Unauthorized', 401);
  }
}
