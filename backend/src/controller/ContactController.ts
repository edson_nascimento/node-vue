import { Request, Response } from 'express';
import { AppException } from '../core/AppException';
import { ContainerDI } from '../core/ContainerDi';
import { ContactService } from '../service/ContactService';

export class ContactController {
  async show(req: Request, res: Response) {
    const service = ContainerDI.get<ContactService>('ContactService', res.locals?.clientConfig);
    const data = await service.findAll();
    return res.json(data);
  }

  async create(req: Request, res: Response) {
    const service = ContainerDI.get<ContactService>('ContactService', res.locals?.clientConfig);
    const { name, cellphone } = req.body;

    const data = await service.create({
      nome: name,
      celular: cellphone,
    });
    return res.json(data);
  }

  async delete(req: Request<{ id: string }>, res: Response) {
    const service = ContainerDI.get<ContactService>('ContactService', res.locals?.clientConfig);
    const id = Number(req.params.id);

    if (!(await service.delete(id))) {
      throw new AppException('Error on delete', 400);
    }
    return res.status(204).send();
  }
}
