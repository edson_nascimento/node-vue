import { Request, Response } from 'express';
import { ContainerDI } from '../core/ContainerDi';
import { TenantService } from '../service/TenantService';

export class TenantController {
  async show(req: Request, res: Response) {
    const tenantService: TenantService = ContainerDI.get<TenantService>('TenantService');

    const data = await tenantService.findAll();

    return res.json(data);
  }
}
