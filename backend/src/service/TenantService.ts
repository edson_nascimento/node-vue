import { Tenant } from '../entity/Tenant';
import { ITenantRepository } from '../repository/ITenantRepository';

export class TenantService {
  constructor(private readonly repository: ITenantRepository) {}

  async findAll() {
    return this.repository.findAll();
  }

  async findByDomain(domain: string): Promise<Tenant> {
    return this.repository.findByDomain(domain);
  }

  async findBySlug(slug: string): Promise<Tenant> {
    return this.repository.findBySlug(slug);
  }

  async findById(id: string): Promise<Tenant> {
    return this.repository.findById(id);
  }
}
