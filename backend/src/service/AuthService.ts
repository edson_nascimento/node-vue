import jwt, { JwtPayload, SignOptions } from 'jsonwebtoken';
import { AppException } from '../core/AppException';
import { getJwtSecret } from '../core/config';
import { Tenant } from '../entity/Tenant';

const signOptions: SignOptions = {
  issuer: 'Mercafacil',
  expiresIn: '24h',
  algorithm: 'HS256',
};
export class AuthService {
  generateToken(payload: JwtPayload): string {
    return jwt.sign(payload, getJwtSecret(), signOptions);
  }

  private decodeToken(token: string) {
    try {
      const decoded = jwt.verify(token, getJwtSecret(), signOptions);
      if (decoded) {
        return decoded;
      }
    } catch (err) {
      console.error(err);
    }
    return '';
  }

  // TODO nao deu tempode criar a tabela no banco
  async login(tenant: Tenant, user: string, password: string): Promise<string> {
    if (user === 'admin' && password === 'admin') {
      return this.generateToken({
        sub: tenant.id,
        aud: tenant.domain,
      });
    }
    throw new AppException('Unauthorized', 401);
  }

  async checkTenantAuth(tenant: Tenant, token: string): Promise<boolean> {
    const decoded = this.decodeToken(token);

    if (decoded && decoded.sub === tenant.id) {
      return true;
    }

    throw new AppException('Unauthorized', 401);
  }
}
