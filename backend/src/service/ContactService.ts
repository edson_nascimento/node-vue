import { AppException } from '../core/AppException';
import { ClientConfig } from '../core/ClientConfig';
import { aplyMask, onlyNumbers } from '../core/masks';
import { Contact } from '../entity/Contact';
import { BaseRepository } from '../repository/BaseRepository';

export class ContactService {
  constructor(private readonly repository: BaseRepository<Contact>, private readonly clientConfig: ClientConfig) {}

  private aplyMasks(contact: Contact): Contact {
    const maskPhone = this.clientConfig.getConfig('phoneMask');
    if (maskPhone) {
      contact.celular = aplyMask(contact.celular, maskPhone);
    }
    if (this.clientConfig.getConfig('nameUppercase')) {
      contact.nome = contact.nome.toUpperCase();
    }

    return contact;
  }

  private validateContact(contact: Contact): Contact {
    contact.celular = onlyNumbers(contact.celular);

    const maxPhoneLength = 13;
    if (contact.celular.length !== maxPhoneLength) {
      throw new AppException('Invalid celular length, size is 13', 400);
    }
    if (!contact.nome.trim()) {
      throw new AppException('Invalid nome length', 400);
    }

    return this.aplyMasks(contact);
  }

  async findAll() {
    return this.repository.findAll();
  }

  async findById(id: number): Promise<Contact> {
    return this.repository.findById(id);
  }

  async create(contact: Contact): Promise<Contact> {
    // Não esta sendo validado se já exite ou se esta no formato correto
    return this.repository.create(this.validateContact(contact));
  }

  async findByPhone(phoneNumber: string): Promise<Contact | undefined> {
    const contacts = await this.repository.find([
      {
        field: 'celular',
        operator: '=',
        value: phoneNumber,
      },
    ]);

    return contacts.shift();
  }

  async delete(id: number): Promise<boolean> {
    const deleted = await this.repository.delete(id);
    return Boolean(deleted);
  }
}
