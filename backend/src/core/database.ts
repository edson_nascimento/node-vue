import Knex from 'knex';

export type ConnectionProps = {
  host: string;
  port: string;
  user: string;
  password: string;
  database: string;
  debug?: boolean;
};

export type DatabaseConfig = {
  client: 'mysql2' | 'pg' | string;
  connection: ConnectionProps;
  pool?: {
    min: number;
    max: number;
  };
  migrations?: {
    directory: string;
  };
};

export type queryCondition = {
  field: string;
  operator: '=' | '<' | '>' | '>=' | '<=';
  value: string | number;
};

function getConnection(config: DatabaseConfig): Knex {
  return Knex(config);
}

function appConnection(): Knex {
  const config: DatabaseConfig = {
    client: 'pg',
    connection: {
      host: process.env.DB_HOST || '',
      port: process.env.DB_PORT || '',
      user: process.env.DB_USER || '',
      password: process.env.DB_PASS || '',
      database: process.env.DB_DATABASE || '',
      debug: false,
    },
    pool: {
      min: 2,
      max: 5,
    },
  };
  return getConnection(config);
}

export function getTenantConnection(config: DatabaseConfig): Knex {
  return getConnection(config);
}

export const mainConnection = appConnection();
