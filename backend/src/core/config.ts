import { AppException } from './AppException';

export function isProduction(): boolean {
  return process.env.NODE_ENV === 'production';
}
export function isModeDeveloper(): boolean {
  return process.env.MODE_DEVELOPER === 'true';
}
export function getJwtSecret(): string {
  const secret = process.env.AUTH_SECRET || '';
  if (secret) {
    return secret;
  }
  throw new AppException('Invalid jwt secret', 500);
}
