import { ContactRepository } from '../repository/ContactRepository';
import { TenantRepository } from '../repository/TenantRepository';
import { AuthService } from '../service/AuthService';
import { ContactService } from '../service/ContactService';
import { TenantService } from '../service/TenantService';
import { ClientConfig } from './ClientConfig';
import { mainConnection } from './database';

const servicesContainer = new Map();
servicesContainer.set('TenantService', () => new TenantService(new TenantRepository(mainConnection)));
servicesContainer.set('AuthService', () => new AuthService());
servicesContainer.set(
  'ContactService',
  (config: ClientConfig) => new ContactService(new ContactRepository(config.getDatabase()), config),
);

type injectKeys = 'TenantService' | 'ContactService' | 'AuthService';
export class ContainerDI {
  private static instances: Map<string, any>;

  private static getInstance(key: string, config?: ClientConfig) {
    if (!this.instances) {
      this.instances = new Map();
    }
    const keyInstance = `${key}_${config?.tenant.slug || ''}`;

    if (!this.instances.has(keyInstance)) {
      const instace = servicesContainer.get(key);

      this.instances.set(keyInstance, instace(config));
    }
    return this.instances.get(keyInstance);
  }

  static get<T>(key: injectKeys, config?: ClientConfig): T {
    return this.getInstance(key, config);
  }
}
