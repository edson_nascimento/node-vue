import msk from 'msk';

export function onlyNumbers(str: string) {
  return str.replace(/\D/g, '');
}

// "+99 (99) 9999-9999", "(99) 9999-9999"
export function aplyMask(str: string, pattern: string) {
  return msk(str, pattern);
}
