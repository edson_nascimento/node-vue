type statusCodeType = 400 | 401 | 404 | 500;
export class AppException extends Error {
  constructor(public message: string, public statusCode: statusCodeType, public returnCode?: number) {
    super(message);
  }
}
