import { Tenant } from '../entity/Tenant';
import { getTenantConnection } from './database';

type confiKeyType = 'nameUppercase' | 'phoneMask';

export class ClientConfig {
  constructor(public tenant: Tenant) {}

  getConfig(key: confiKeyType): any {
    return typeof this.tenant.configs === 'object' ? this.tenant.configs[key] : undefined;
  }

  getDatabase() {
    return getTenantConnection(this.tenant.database);
  }
}
