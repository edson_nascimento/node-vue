import { NextFunction, Request, Response } from 'express';
import { AppException } from '../core/AppException';
import { ClientConfig } from '../core/ClientConfig';
import { ContainerDI } from '../core/ContainerDi';
import { Tenant } from '../entity/Tenant';
import { AuthService } from '../service/AuthService';
import { TenantService } from '../service/TenantService';

export function getBearerToken(req: Request): string {
  const token = req.headers['x-access-token'] || req.headers.authorization;

  if (!token || typeof token !== 'string' || !token.startsWith('Bearer ') || token.length < 8) {
    throw new AppException('Invalid Bearer Token', 401);
  }

  return token.slice(7, token.length);
}

export async function getTenantRequest(req: Request): Promise<Tenant | undefined> {
  const tenantService = ContainerDI.get<TenantService>('TenantService');

  let tenant = await tenantService.findByDomain(req.hostname);

  if (!tenant && typeof req.query.tenant === 'string') {
    tenant = await tenantService.findBySlug(req.query.tenant);
  }

  return tenant;
}

export async function verifyClientAuth(req: Request, res: Response, next: NextFunction) {

  const tenant = await getTenantRequest(req);

  if (!tenant) {
    throw new AppException('Invalid Tenant', 401);
  }

  const authService = ContainerDI.get<AuthService>('AuthService');

  await authService.checkTenantAuth(tenant, getBearerToken(req));

  res.locals.clientConfig = new ClientConfig(tenant);

  return next();
}
