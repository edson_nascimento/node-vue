import { NextFunction, Request, Response } from 'express';
import { AppException } from '../core/AppException';
import { isProduction } from '../core/config';

const STATUS_CODE = {
  INTERNAL_SERVER_ERROR: 500,
};

type ErrorResponse = {
  message: string;
  statusCode: number;
  returnCode?: number;
};

export function globalErrorHandle(error: Error, request: Request, response: Response, next: NextFunction) {
  const message = isProduction()
    ? 'Ocorreu um erro no servidor, se o problema persistir, entre em contato conosco!'
    : error.message;

  let exception: ErrorResponse;

  if (error instanceof AppException) {
    exception = {
      message: error.message,
      statusCode: error.statusCode,
      returnCode: error.returnCode || 0,
    };
  } else {
    exception = {
      message,
      statusCode: STATUS_CODE.INTERNAL_SERVER_ERROR,
      returnCode: 0,
    };
  }

  return response.status(exception.statusCode).json(exception);
}
