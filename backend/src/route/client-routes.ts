import Router from 'express-promise-router';
import { verifyClientAuth } from '../middleware/auth';
import { ContactController } from '../controller/ContactController';

const clientRoutes = Router();
const contactController = new ContactController();

clientRoutes.use(verifyClientAuth);
clientRoutes.get('/contacts', contactController.show);
clientRoutes.post('/contacts', contactController.create);
clientRoutes.delete('/contacts/:id', contactController.delete);

export { clientRoutes };
