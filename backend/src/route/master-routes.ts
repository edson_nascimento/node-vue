import Router from 'express-promise-router';
import { TenantController } from '../controller/TenantController';

const masterRoutes = Router();
const tenantController = new TenantController();

// não tem autenticação só para listar mesmo
masterRoutes.get('/tenants', tenantController.show);

export { masterRoutes };
