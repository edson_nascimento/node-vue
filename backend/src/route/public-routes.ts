import Router from 'express-promise-router';
import { AuthController } from '../controller/AuthController';

const publicRoutes = Router();
const authController = new AuthController();

publicRoutes.post('/auth', authController.clientLogin);

export { publicRoutes };
