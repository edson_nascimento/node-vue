import Router from 'express-promise-router';
import { clientRoutes } from './client-routes';
import { masterRoutes } from './master-routes';
import { publicRoutes } from './public-routes';

const routes = Router();

routes.use(publicRoutes);
routes.use('/master', masterRoutes);
routes.use('/client', clientRoutes);

export { routes };
