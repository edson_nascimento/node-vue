import express from 'express';
import morgan from 'morgan';
import cors from 'cors';
import '../core/env-config';
import { globalErrorHandle } from '../middleware/error-handler';
import { routes } from '../route';
import { isModeDeveloper } from '../core/config';

export const server = express();

if (isModeDeveloper()) {
  server.use(morgan('dev'));
  server.set('json spaces', 2);
}

server.use(express.json());
server.use(cors());
server.use(routes);
server.use(globalErrorHandle);
