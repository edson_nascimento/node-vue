import { isModeDeveloper } from '../core/config';
import { server } from './app';

const PORT = process.env.APP_PORT || 3333;

server.listen(PORT, () => {
  // eslint-disable-next-line no-unused-expressions
  isModeDeveloper() && console.log(`Server running on port ${PORT} \n`);
});
