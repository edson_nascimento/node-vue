import { DatabaseConfig } from '../core/database';

type configType = {
  nameUppercase: boolean;
  phoneMask: string;
};
export interface ITenant {
  id?: string;
  slug: string;
  domain: string;
  database: DatabaseConfig;
  configs?: configType;
}
export class Tenant implements ITenant {
  id: string;

  slug: string;

  domain: string;

  database: DatabaseConfig;

  configs?: configType;

  constructor(id: string, slug: string, domain: string, database: DatabaseConfig, configs?: configType) {
    this.id = id;
    this.slug = slug;
    this.domain = domain;
    this.database = database;
    this.configs = configs;
  }
}
