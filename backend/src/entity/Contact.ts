export interface IContact {
  id?: number;
  nome: string;
  celular: string;
}

export class Contact implements IContact {
  id?: number | undefined;

  nome: string;

  celular: string;

  constructor(nome: string, celular: string, id?: number) {
    this.id = id;
    this.nome = nome;
    this.celular = celular;
  }
}
