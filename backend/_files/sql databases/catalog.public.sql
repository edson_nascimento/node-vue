/*
 Navicat Premium Data Transfer

 Source Server         : Postgre docker
 Source Server Type    : PostgreSQL
 Source Server Version : 130004
 Source Host           : localhost:5432
 Source Catalog        : catalog
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 130004
 File Encoding         : 65001

 Date: 22/08/2021 21:35:09
*/


-- ----------------------------
-- Table structure for tenant
-- ----------------------------
DROP TABLE IF EXISTS "public"."tenant";
CREATE TABLE "public"."tenant" (
  "id" uuid NOT NULL DEFAULT gen_random_uuid(),
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "database" jsonb NOT NULL,
  "configs" jsonb NOT NULL,
  "domain" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "slug" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of tenant
-- ----------------------------
INSERT INTO "public"."tenant" VALUES ('2c2e68d5-3603-4fa4-8324-5dba11732195', 'VareJão', '{"pool": {"max": 5, "min": 2}, "client": "pg", "connection": {"host": "localhost", "port": "5432", "user": "admin", "debug": false, "database": "varejao", "password": "admin"}}', '{"phoneMask": "9999999999999", "nameUppercase": false}', 'varejao.test', 'varejao');
INSERT INTO "public"."tenant" VALUES ('3b0efb66-ef93-4e2f-89b8-0936afe8b667', 'Macapá', '{"pool": {"max": 5, "min": 2}, "client": "mysql2", "connection": {"host": "localhost", "port": "3306", "user": "root", "debug": false, "database": "macapa", "password": "admin"}}', '{"phoneMask": "+99 (99) 9999-9999", "nameUppercase": true}', 'macapa.test', 'macapa');

-- ----------------------------
-- Indexes structure for table tenant
-- ----------------------------
CREATE UNIQUE INDEX "unique_domain" ON "public"."tenant" USING btree (
  "domain" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE UNIQUE INDEX "unique_id" ON "public"."tenant" USING btree (
  "id" "pg_catalog"."uuid_ops" ASC NULLS LAST
);
CREATE INDEX "unique_slug" ON "public"."tenant" USING btree (
  "slug" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table tenant
-- ----------------------------
ALTER TABLE "public"."tenant" ADD CONSTRAINT "tenant_pkey" PRIMARY KEY ("id");
