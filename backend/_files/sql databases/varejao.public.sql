/*
 Navicat Premium Data Transfer

 Source Server         : Postgre docker
 Source Server Type    : PostgreSQL
 Source Server Version : 130004
 Source Host           : localhost:5432
 Source Catalog        : varejao
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 130004
 File Encoding         : 65001

 Date: 22/08/2021 21:35:25
*/


-- ----------------------------
-- Sequence structure for contacts_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."contacts_id_seq";
CREATE SEQUENCE "public"."contacts_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Table structure for contacts
-- ----------------------------
DROP TABLE IF EXISTS "public"."contacts";
CREATE TABLE "public"."contacts" (
  "id" int4 NOT NULL DEFAULT nextval('contacts_id_seq'::regclass),
  "nome" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "celular" varchar(13) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Records of contacts
-- ----------------------------

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."contacts_id_seq"
OWNED BY "public"."contacts"."id";
SELECT setval('"public"."contacts_id_seq"', 20, true);

-- ----------------------------
-- Primary Key structure for table contacts
-- ----------------------------
ALTER TABLE "public"."contacts" ADD CONSTRAINT "contacts_pkey" PRIMARY KEY ("id");
