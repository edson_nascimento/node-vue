import { aplyMask, onlyNumbers } from '../../src/core/masks';

test('Deve aplicar mascara', () => {
  expect(aplyMask('5541993278935', '+99 (99) 9999-9999')).toEqual('+55 (41) 9932-78935');
});

test('Deve retirar tudo que não for digito', () => {
  expect(onlyNumbers(' +55 (4 1) 9932-789 35 ')).toEqual('5541993278935');
});
