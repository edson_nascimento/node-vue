import { Request } from 'express';
import { getBearerToken } from '../../src/middleware/auth';

test('Deve pegar o bearer token do header', () => {
  const token = 'xxxxxxxxxx';
  const mockRequest = <Request>{
    headers: {
      authorization: `Bearer ${token}`,
    },
  };
  expect(getBearerToken(mockRequest)).toEqual(token);
});

test('Bearer token incorreto', () => {
  const mockRequest = <Request>{
    headers: {
      authorization: 'xxxxxxxxxx invalid',
    },
  };
  expect(() => {
    getBearerToken(mockRequest);
  }).toThrow();
});
