import supertest from 'supertest';
import { server } from '../../src/main/app';

const request = supertest(server);

test('Não deve fazer login', async () => {
  const response = await request.post('/auth?tenant=macapa').send({
    user: 'erro',
    password: 'erro',
  });
  expect(response.statusCode).toEqual(401);
});

test('Deve fazer login', async () => {
  const response = await request.post('/auth?tenant=macapa').send({
    user: 'admin',
    password: 'admin',
  });
  expect(response.statusCode).toEqual(200);
  expect(response.body).toHaveProperty('accessToken');
});
