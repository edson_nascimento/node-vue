import supertest from 'supertest';
import { server } from '../../src/main/app';

describe('Perfil Routes', () => {
  let request = supertest(server);
  let token: string;

  beforeAll((done) => {
    request = supertest(server);
    request
      .post('/auth?tenant=macapa')
      .send({
        user: 'admin',
        password: 'admin',
      })
      .end((err, response) => {
        token = response.body.accessToken;
        done();
      });
  });

  it('Deve inserir um contato', async () => {
    const response = await request
      .post('/client/contacts?tenant=macapa')
      .send({
        name: 'Edson',
        cellphone: '+55(41) 99327-8935',
      })
      .auth(token, { type: 'bearer' });

    expect(response.statusCode).toEqual(200);
    expect(response.body).toHaveProperty('id');
  });

  it('Deve listar os contatos', async () => {
    const response = await request.get('/client/contacts?tenant=macapa').auth(token, { type: 'bearer' });

    expect(response.statusCode).toEqual(200);
    expect(Array.isArray(response.body)).toBe(true);
  });
});
