import supertest from 'supertest';
import { server } from '../../src/main/app';

const request = supertest(server);

test('Deve buscar os tenants atuais', async () => {
  const response = await request.get('/master/tenants');
  expect(response.statusCode).toEqual(200);
  expect(Array.isArray(response.body)).toBe(true);
  expect(response.body.length).toBe(2);
});
