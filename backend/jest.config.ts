// Configuration options: https://jestjs.io/pt-BR/docs/configuration
import type { Config } from '@jest/types';

const config: Config.InitialOptions = {
  bail: true,
  clearMocks: true,
  collectCoverageFrom: ['src/**/*.ts'],
  coveragePathIgnorePatterns: ['test/mocks'],
  coverageDirectory: 'test/coverage',
  moduleFileExtensions: ['ts', 'js'],
  testEnvironment: 'node',
};

export default config;
